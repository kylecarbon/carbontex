\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{carbonarticle}[2018/02/10 CarbonArticle]

\LoadClass{article}
%---------------------- PACKAGES ----------------------%
\RequirePackage{carbonformatting}
\RequirePackage{carboncode}
\RequirePackage{carbonpackages}

\graphicspath{{\string~/texmf/tex/latex/carbontex/figures/}}
\def\smallcompanylogo{\includegraphics[height=0.35cm]{shieldai_icon_monochrome.pdf}}
\def\smallpersonallogo{\includegraphics[height=0.5cm]{kcarbon_avatar}}
%% Define custom page styles
\fancypagestyle{shieldPageStyle}{
    \fancyhf{}
    \fancyhead[L]{\small \leftmark}
    \fancyhead[R]{\small \rightmark}
    \fancyfoot[C]{{\smallcompanylogo}}
    \fancyfoot[L]{\small Copyright \copyright{} Shield AI \the\year}
    \fancyfoot[R]{\small \thepage\ of \pageref{LastPage}}
    \renewcommand{\headrulewidth}{1pt}
    \renewcommand{\footrulewidth}{1pt}
}

\fancypagestyle{shieldTocPageStyle}{
    \fancyhf{}
    \fancyfoot[L]{\small Copyright \copyright{} Shield AI \the\year}
    \fancyfoot[C]{{\smallcompanylogo}}
    \fancyfoot[R]{\small \thepage}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{1pt}
}

\fancypagestyle{carbonPageStyle}{
    \fancyhf{}
    \fancyhead[L]{\small \leftmark}
    \fancyhead[R]{\small \rightmark}
    \fancyfoot[C]{{\smallpersonallogo}}
    \fancyfoot[L]{\small Copyright \copyright{} Kyle Carbon \the\year}
    \fancyfoot[R]{\small \thepage\ of \pageref{LastPage}}
    \renewcommand{\headrulewidth}{1pt}
    \renewcommand{\footrulewidth}{1pt}
}

\fancypagestyle{carbonTocPageStyle}{
    \fancyhf{}
    \fancyfoot[L]{\small Copyright \copyright{} Kyle Carbon \the\year}
    \fancyfoot[C]{{\smallpersonallogo}}
    \fancyfoot[R]{\small \thepage}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{1pt}
}

\renewcommand{\subsectionmark}[1]{\markright{\thesubsection~- ~#1}}

\renewcommand\lstlistingname{Algorithm}
\renewcommand\lstlistlistingname{Algorithms}
\def\lstlistingautorefname{Algorithm}

\endinput