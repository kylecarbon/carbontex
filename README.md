# Setup
Usage: `./carbontex_config -h`
## Install
```
pip3 install click
git clone https://gitlab.com/kylecarbon/carbontex.git
./carbontex_config -i
```
## Uninstall
```
./carbontex_config -u
```